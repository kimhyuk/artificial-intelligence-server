import models from "../../models";
import debug from "debug";
import validator from "validator";
import { ValidationError, UserInputError } from "apollo-server";
import jsonwebtoken from "jsonwebtoken";
import sotreUplaod from "../../utils/storeUpload";

const _debug = debug("gql:resolver");

export default {
	Query: {
		async uploads() {
			return Promise.resolve(models.FileModel.find());
		}
	},
	Mutation: {
		async singleUpload(_, { file }) {
			return Promise.resolve(models.FileModel.upload(await file));
		},
		async multipleUpload(_, { files }) {
			return Promise.resolve(models.FileModel.find());
		}
	}
};
