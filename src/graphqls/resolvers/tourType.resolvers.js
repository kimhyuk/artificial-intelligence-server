import models from "../../models";
import debug from "debug";
import {
	ValidationError,
	UserInputError,
	AuthenticationError
} from "apollo-server";
import jsonwebtoken from "jsonwebtoken";

const _debug = debug("gql:resolver");

export default {
	Query: {
		tourTypes: async () => {
			return models.TourTypeModel.find();
		}
	},
	Mutation: {
		insertTourType: async (_, tourTypeForm, { req }) => {
			try {
				await models.TourTypeModel.create(tourTypeForm);
				return Promise.resolve(true);
			} catch (err) {
				throw err;
			}
		},
		deleteTourType: async (_, { _id }, { req }) => {
			try {
				await models.TourTypeModel.findByIdAndDelete(_id);
				return Promise.resolve(true);
			} catch (err) {
				throw err;
			}
		}
	}
};
