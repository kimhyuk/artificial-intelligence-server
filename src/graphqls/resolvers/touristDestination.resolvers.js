import models from "../../models";
import debug from "debug";
import {
	ValidationError,
	UserInputError,
	AuthenticationError
} from "apollo-server";
import _ from "lodash";
import { isArray } from "util";

const _debug = debug("gql:resolver");

export default {
	Query: {
		touristDestinations: async (_, { id, page }) => {
			try {
				if (!page) {
					page = 0;
				}
				const doc = await models.TouristDestinationModel.find()
					.skip(page * 12)
					.limit(12)
					.populate("tourType")
					.populate("file");
				_debug(doc);
				return doc;
			} catch (err) {
				throw err;
			}
		},
		touristDestinationCores: async (obj, { id }) => {
			try {
				const docs = await models.UserModel.find({}, { votes: 1 });

				let scores = await Promise.all(
					_.map(docs, async user => {
						const idx = _.findIndex(user.votes, vote => {
							return vote.touristDestination.toString() === id;
						});

						if (idx !== -1) {
							return Math.ceil(user.votes[idx].score);
						}
					})
				);

				_.remove(scores, v => v === undefined);
				let result = [0, 0, 0, 0, 0];
				for (let idx = 0; idx < scores.length; idx++) {
					result[scores[idx] - 1]++;
				}

				return result;
			} catch (err) { }
		},
		averageVote: async (obj, { id }) => {
			try {
				let sum = 0;
				const docs = await models.UserModel.find({}, { votes: 1 });

				let scores = await Promise.all(
					_.map(docs, async user => {
						const idx = _.findIndex(user.votes, vote => {
							return vote.touristDestination.toString() === id;
						});

						if (idx !== -1) {
							return user.votes[idx].score;
						}
					})
				);
				if (!isArray(scores)) {
					scores = [];
				}
				_.remove(scores, v => v === undefined);
				for (let idx in scores) {
					sum += scores[idx];
				}
				return scores.length === 0 ? 0 : sum / scores.length;
			} catch (err) { }
		}
	},
	Mutation: {
		insertTouristDestination: async (req, { touristDestination }) => {
			try {
				delete touristDestination._id;
				await models.TouristDestinationModel.create(touristDestination);
				return Promise.resolve(true);
			} catch (err) {
				throw err;
			}
		}
	}
};
