import models from "../../models";
import debug from "debug";
import {
	ValidationError,
	UserInputError,
	AuthenticationError
} from "apollo-server";
import jsonwebtoken from "jsonwebtoken";
import _ from "lodash";
import { spawn } from "child_process";
import path from "path";

const pyPath = path.join(require.main.filename, "../../userBasedRecommend.py");

const _debug = debug("gql:resolver");

export default {
	Query: {
		user: async (_, { id }) => {
			const user = await models.UserModel.findById(id);
			return user;
		},
		users: async () => {
			const users = await models.UserModel.find();
			return users;
		},
		isLogin: async (_, args, req) => {
			return req.user ? true : false;
		},
		votes: async (obj, args, req) => {
			const doc = await models.UserModel.findById(req.user._id, {
				votes: 1
			})
				.populate("votes.touristDestination")
				.populate("file");

			const votes = await Promise.all(
				_.map(doc.votes, async value => {
					value.touristDestination = await models.TouristDestinationModel.populate(
						value.touristDestination,
						{ path: "tourType" }
					);
					value.file = await models.TouristDestinationModel.populate(
						value.touristDestination,
						{ path: "file" }
					);

					return value;
				})
			);

			return votes;
		},
		vote: async (obj, args, req) => {
			const doc = await models.UserModel.findById(req.user._id, {
				votes: 1
			});

			const idx = _.findIndex(doc.votes, vote => {
				return (
					vote.touristDestination.toString() === args.touristDestination_id
				);
			});

			return idx !== -1 ? doc.votes[idx] : { score: 0, _id: "" };
		},
		predictionVote: async (obj, args, req) => {
			const doc = await models.UserModel.findById(req.user._id, {
				predictionVotes: 1
			});

			const idx = _.findIndex(doc.predictionVotes, vote => {
				return (
					vote.touristDestination.toString() === args.touristDestination_id
				);
			});

			_debug(idx);

			return idx !== -1 ? doc.predictionVotes[idx] : null;
		},
		predictionVotes: async (obj, { page }, req) => {
			try {
				if (!page) {
					page = 0;
				}
				const doc = await models.UserModel.findById(req.user._id, {
					predictionVotes: { $slice: [page * 12, 12] }
				});

				let ids = await Promise.all(
					_.map(doc.predictionVotes, async vote => {
						return vote.touristDestination;
					})
				);

				const tours = await models.TouristDestinationModel.find({
					_id: { $in: ids }
				})
					.populate("tourType")
					.populate("file");

				let results = await Promise.all(
					_.map(ids, async id => {
						const idx = _.findIndex(tours, ["_id", id]);

						return idx !== -1 ? tours[idx] : undefined;
					})
				);

				_.remove(results, v => v === undefined);
				return results;
			} catch (err) {
				throw err;
			}
		},
		my: async (obj, { }, req) => {
			const doc = await models.UserModel.findById(req.user._id, {
				votes: 1
			})
				.populate("votes.touristDestination")
				.populate("file");

			let averageScore = 0;

			let arr = [
				"0",
				"0.5",
				"1",
				"1.5",
				"2",
				"2.5",
				"3",
				"3.5",
				"4",
				"4.5",
				"5"
			];
			let scores = new Array(arr.length);
			scores.fill(0);
			let max = 0;
			let cities = new Object();
			let tourTypes = new Object();
			const votes = await Promise.all(
				_.map(doc.votes, async value => {
					averageScore += value.score;
					value.touristDestination = await models.TouristDestinationModel.populate(
						value.touristDestination,
						{ path: "tourType" }
					);
					value.file = await models.TouristDestinationModel.populate(
						value.touristDestination,
						{ path: "file" }
					);

					if (value.touristDestination) {
						if (cities.hasOwnProperty(value.touristDestination.city)) {
							cities[value.touristDestination.city]++;
						} else {
							cities[value.touristDestination.city] = 0;
						}
						if (value.touristDestination.tourType) {
							if (
								tourTypes.hasOwnProperty(value.touristDestination.tourType.name)
							) {
								tourTypes[value.touristDestination.tourType.name]++;
							} else {
								tourTypes[value.touristDestination.tourType.name] = 0;
							}
						}
					}
					const f = _.findIndex(arr, o => Number(o) === value.score);
					scores[f]++;
					let tmp = _.max(scores);
					if (max < tmp) max = tmp;
					return value;
				})
			);

			let words = await Promise.all(
				_.map(cities, async (value, key) => {
					return [key, value];
				})
			);
			words = words.concat(
				await Promise.all(
					_.map(tourTypes, async (value, key) => {
						return [key, value];
					})
				)
			);

			words = _.sortBy(words, [
				function (o) {
					return -o[1];
				}
			]);

			words = words.slice(0, (words.length / 3) * 2);
			const f = _.findIndex(scores, o => o === max);

			averageScore = averageScore / votes.length;
			return {
				votes,
				averageScore,
				scores,
				higherScore: arr[f],
				words
			};
		}
	},
	Mutation: {
		signup: async (req, signupForm) => {
			try {
				const user = await models.UserModel.signup(signupForm);
				return Promise.resolve(user.token);
			} catch (err) {
				throw err;
			}
		},
		login: async (_, loginForm) => {
			const user = await models.UserModel.login(loginForm);
			return Promise.resolve(user.token);
		},
		addVote: async (__, { touristDestination_id, score }, req) => {
			try {
				const user = await models.UserModel.findById(req.user._id, {
					votes: 1
				});
				const idx = _.findIndex(user.votes, function (o) {
					return o.touristDestination == touristDestination_id;
				});

				if (idx === -1) {
					const touristDestination = await models.TouristDestinationModel.findById(
						touristDestination_id
					);
					if (!touristDestination) return Promise.resolve(false);

					user.votes.push({ touristDestination: touristDestination_id, score });
				} else {
					user.votes[idx].score = score;
				}

				await user.save();

				spawn("python3.5", [pyPath, user._id]);
				return Promise.resolve(true);
			} catch (err) {
				throw err;
			}
		}
	}
};
