import config from "../server.config";
import { GraphQLServer } from "graphql-yoga";
import path from "path";
import express from "express";
import graphqlProp from "./graphqls";
import db from "./utils/db";
import passport from "passport";
import authenticate from "./passport/authenticate";
import debug from "debug";
import passport_jwt from "./passport/jwt";
import fallback from "connect-history-api-fallback";
import helmet from "helmet";
import fileSecret from "./middlewares/fileSecret";

const _debug = debug("app");

const dbOptions = {
	host: process.env.DB_HOST,
	port: process.env.DB_PORT,
	username: process.env.DB_USERNAME,
	password: process.env.DB_PASSWORD
};
const serverOptions = {
	port: process.env.PORT || 4000,
	endpoint: process.env.GRAPHQL_ENDPOINT,
	subscriptions: process.env.GRAPHQL_SUBSCRIPTIONS,
	playground: process.env.GRAPHQL_PLAYGROUND,
	uploads: {
		maxFiledSize: 10000,
		maxFileSize: 100000000,
		maxFiles: 5
	}
};

const configPath = config.getPath();

db(dbOptions, { useNewUrlParser: true, dbName: process.env.DB_DATABASE_NAME });

const server = new GraphQLServer({
	typeDefs: graphqlProp.typeDefs,
	resolvers: graphqlProp.resolvers,
	middlewares: graphqlProp.middlewares,
	context: ({ request }) => ({
		user: request.user
	})
});

const fallbackMiddleware = fallback({
	verbose: true
});

server.express.disable("x-powerd-by");
server.use(helmet());
passport.use(passport_jwt);

server.use((req, res, next) => {
	switch (req.path) {
		case serverOptions.endpoint:
		case serverOptions.playground:
		case serverOptions.subscriptions:
			next();
			break;
		default:
			if (!/\/uploads\//i.test(req.path)) {
				fallbackMiddleware(req, res, next);
			} else {
				next();
			}
			break;
	}
});
server.use(express.static(configPath.static));

server.use(
	"/uploads",
	authenticate,
	fileSecret,
	express.static(configPath.uploads)
);
server.use(process.env.GRAPHQL_ENDPOINT, authenticate);
server.start(serverOptions, () =>
	console.log("Server is running on localhost" + serverOptions.port)
);
server.get("/", (req, res, next) => {
	// 해당 위치에 없을 경우 처리해야됨
	const indexHtml = path.join(configPath.static, "index.html");
	res.sendFile(indexHtml);
});
