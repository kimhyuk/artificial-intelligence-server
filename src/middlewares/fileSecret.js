import debug from "debug";
import path from "path";

const _debug = debug("middlewares:fileSecret");

export default (req, res, next) => {
	if (!/\/uploads\/secret\/[\w|\/]+/i.test(req.originalUrl)) {
		_debug("general");
		return next();
	}
	if (!req.user) {
		_debug("not login");

		return next(new Error());
	}

	const parse = path.parse(req.originalUrl);
	const match = parse.dir.match(/[a-f\d]{24}/i);

	if (match == null) return next(new Error("not mongoId"));
	if (match[0] !== req.user._id) return next(new Error("id not match"));

	next();
};
