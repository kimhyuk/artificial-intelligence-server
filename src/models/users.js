import mongoose from "mongoose";
import hasher from "../utils/hasher";
import debug from "debug";
import validator from "validator";
import jwt from "jsonwebtoken";

const _debug = debug("models:users");

const voteSchema = mongoose.Schema({
	touristDestination: {
		type: mongoose.Types.ObjectId,
		ref: "TouristDestination",
		unique: true
	},
	score: { type: Number, index: true }
});

//데이터 모델링
const userSchema = mongoose.Schema({
	email: { type: String, unique: true, required: true },
	username: { type: String, unique: true, required: true },
	password: { type: String, required: true },
	admin: { type: Boolean, default: false, required: true },
	salt: { type: String, required: true },
	auths: [],
	votes: [voteSchema],
	predictionVotes: [voteSchema]
});

class UserClass {
	static async signup({ email, username, password }) {
		return this.create({
			email,
			username,
			password
		});
	}
	static async login({ email, password }) {
		let user = undefined;
		if (validator.isEmail(email)) {
			user = await this.findByUserEmail(email);
		} else {
			user = await this.findByEmailorUsername(email);
		}

		if (!user) {
			throw new Error("이메일 혹은 이름이 올바르지 않습니다.");
		}

		if (!(await user.verifyPassword(password))) {
			throw new Error("비밀번호가 올바르지 않습니다.");
		}

		return Promise.resolve(user);
	}
	static findByUsername(username) {
		return this.findOne({ username: username });
	}

	static findByUserEmail(email) {
		return this.findOne({ email: email });
	}

	static findByEmailorUsername(email) {
		if (validator.isEmail(email)) {
			return this.findByUserEmail(email);
		} else {
			return this.findByUsername(email);
		}
	}

	static generateHashAndSalt(password) {
		return hasher({ password });
	}

	static async create({ email, username, password }) {
		if (!validator.isEmail(email)) {
			throw new Error("이메일 형식이 옳지 않습니다.");
		} else if (typeof username !== "string") {
			throw new Error("이름이 옳지 않습니다.");
		} else if (!validator.isLength(password, { min: 8, max: 16 })) {
			throw new Error("비밀번호 길이는 8~16자 입니다.");
		}

		const { hash, salt } = await this.generateHashAndSalt(password);

		const user = new this({
			email: email.trim(),
			username: username.trim(),
			password: hash,
			salt
		});

		return user.save();
	}

	get token() {
		const token = jwt.sign(
			{
				_id: this._id,
				email: this.email,
				username: this.username
			},
			process.env.JWT_SECRET,
			{ expiresIn: "1y" }
		);

		return token;
	}

	async verifyPassword(unauthorizedPassword) {
		const { password, salt } = this;
		try {
			const { hash } = await hasher({ password: unauthorizedPassword, salt });
			return Promise.resolve(hash === password);
		} catch (err) {
			console.error(err);
			return Promise.reject(err);
		}
	}
}

userSchema.post("save", async function (error, doc) {
	if (error.name === "MongoError" && error.code === 11000) {
		return Promise.reject(new Error("중복된 이메일 혹은 이름이 있습니다."));
	} else {
		return Promise.resolve();
	}
});

userSchema.loadClass(UserClass);
const userModel = mongoose.model("User", userSchema);

userModel.findByUsername("admin").then(user => {
	if (!user) {
		userModel
			.create({
				email: "admin@admin.com",
				username: "admin",
				password: "admin15924"
			})
			.then(user => {
				user.admin = true;
				user.save();
			});
	}
});

export default userModel;
