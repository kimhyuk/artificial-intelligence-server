import mongoose from "mongoose";

//데이터 모델링
const authSchema = mongoose.Schema({
    authName: { type: String, unique: true }
});


const authModel = mongoose.model("Auth", authSchema);


export default authModel;