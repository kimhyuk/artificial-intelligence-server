import UserModel from "./users";
import FileModel from "./files";
import TouristDestinationModel from "./touristDestinations";
import CounterModel from "./counter";
import TourTypeModel from "./tourTypes";

export default {
	UserModel,
	FileModel,
	TourTypeModel,
	TouristDestinationModel,
	CounterModel
};
