import mongoose from "mongoose";
import hasher from "../utils/hasher";
import debug from "debug";
import validator from "validator";
import jwt from "jsonwebtoken";

const _debug = debug("models", "tours");

//데이터 모델링
const touristDestinationSchema = mongoose.Schema({
	name: { type: String },
	file: { type: mongoose.Types.ObjectId, ref: "File", required: true },
	tourType: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "TourType",
		required: true,
		index: true
	},
	location: { type: String },
	city: { type: String },
	average: {
		score: { type: Number, default: 0 },
		reviewCount: { type: Number, default: 0 }
	},
	tagCount: { type: String, default: 0 }
});

class TouristDetinationClass { }

touristDestinationSchema.loadClass(TouristDetinationClass);
const touristDestinationModel = mongoose.model(
	"TouristDestination",
	touristDestinationSchema
);

export default touristDestinationModel;
