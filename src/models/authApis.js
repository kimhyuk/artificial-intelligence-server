import mongoose from "mongoose";


const authApiSchema = mongoose.Schema({
    auth_ids: [
        {
            _id: { type: mongoose.Types.ObjectId, unique: true }
        }
    ],
    apiName: { type: String, unique: true }

});

const authApiModel = mongoose.model("AuthApi", authApiSchema);


export default authApiModel;