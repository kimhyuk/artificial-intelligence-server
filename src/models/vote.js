import mongoose from "mongoose";
import hasher from "../utils/hasher";
import debug from "debug";
import validator from "validator";
import jwt from "jsonwebtoken";

const _debug = debug("models", "votes");

//데이터 모델링
const voteSchema = mongoose.Schema({
	email: { type: String, unique: true, required: true },
	votename: { type: String, unique: true, required: true },
	password: { type: String, required: true },
	admin: { type: Boolean, default: false, required: true },
	salt: { type: String, required: true },
	auths: []
});

class VoteClass {
	static async signup({ email, votename, password }) {
		return this.create({
			email,
			votename,
			password
		});
	}
	static async login({ email, password }) {
		let vote = undefined;
		if (validator.isEmail(email)) {
			vote = await this.findByvoteEmail(email);
		} else {
			vote = await this.findByEmailorvotename(email);
		}

		if (!vote) {
			throw new Error("이메일 혹은 이름이 올바르지 않습니다.");
		}

		if (!(await vote.verifyPassword(password))) {
			throw new Error("비밀번호가 올바르지 않습니다.");
		}

		return Promise.resolve(vote);
	}
	static findByvotename(votename) {
		return this.findOne({ votename: votename });
	}

	static findByvoteEmail(email) {
		return this.findOne({ email: email });
	}

	static findByEmailorvotename(email) {
		if (validator.isEmail(email)) {
			return this.findByvoteEmail(email);
		} else {
			return this.findByvotename(email);
		}
	}

	static generateHashAndSalt(password) {
		return hasher({ password });
	}

	static async create({ email, votename, password }) {
		if (!validator.isEmail(email)) {
			throw new Error("이메일 형식이 옳지 않습니다.");
		} else if (typeof votename !== "string") {
			throw new Error("이름이 옳지 않습니다.");
		} else if (!validator.isLength(password, { min: 8, max: 16 })) {
			throw new Error("비밀번호 길이는 8~16자 입니다.");
		}

		const { hash, salt } = await this.generateHashAndSalt(password);

		const vote = new this({
			email: email.trim(),
			votename: votename.trim(),
			password: hash,
			salt
		});

		return vote.save();
	}

	get token() {
		const token = jwt.sign(
			{
				_id: this._id,
				email: this.email,
				votename: this.votename
			},
			process.env.JWT_SECRET,
			{ expiresIn: "1y" }
		);

		return token;
	}

	async verifyPassword(unauthorizedPassword) {
		const { password, salt } = this;
		try {
			const { hash } = await hasher({ password: unauthorizedPassword, salt });
			return Promise.resolve(hash === password);
		} catch (err) {
			console.error(err);
			return Promise.reject(err);
		}
	}
}

voteSchema.post("save", async function(error, doc) {
	if (error.name === "MongoError" && error.code === 11000) {
		return Promise.reject(new Error("중복된 이메일 혹은 이름이 있습니다."));
	} else {
		return Promise.resolve();
	}
});

voteSchema.loadClass(VoteClass);
const voteModel = mongoose.model("vote", voteSchema);

export default voteModel;
