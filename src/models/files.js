import mongoose from "mongoose";
import storeUpload from "../utils/storeUpload";
import debug from "debug";

const _debug = debug("models:files");

//데이터 모델링
const fileSchema = mongoose.Schema({
	originalName: { type: String },
	filename: { type: String, unique: true },
	mimetype: { type: String },
	encoding: { type: String },
	path: { type: String }
});

class FileClass {
	static async upload(upload) {
		const { createReadStream, mimetype, encoding } = upload;
		const originalName = upload.filename;
		const { filename, path } = await storeUpload({ createReadStream });
		const file = new this({ originalName, filename, mimetype, encoding, path });
		return file.save();
	}

	static async uploads() { }
}

fileSchema.loadClass(FileClass);
const fileModel = mongoose.model("File", fileSchema);

export default fileModel;
