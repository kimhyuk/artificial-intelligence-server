import mongoose from "mongoose";
import hasher from "../utils/hasher";
import debug from "debug";
import validator from "validator";
import jwt from "jsonwebtoken";

const _debug = debug("models", "tours");

//데이터 모델링
const counterSchema = mongoose.Schema({
	_id: { type: String, required: true },
	seq: { type: Number, default: 0 }
});

class CounterClass {
	static async getSec(_id) {
		let counter = await this.findOne({ _id });

		if (!counter) {
			counter = new this({ _id });
			await counter.save();
		}

		return Promise.resolve(counter.seq);
	}

	static async increament(_id) {
		await this.getSec(_id);
		return this.findByIdAndUpdate({ _id }, { $inc: { seq: 1 } });
	}
}

counterSchema.loadClass(CounterClass);
const counterModel = mongoose.model("Counter", counterSchema);

export default counterModel;
