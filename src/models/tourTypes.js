import mongoose from "mongoose";
import debug from "debug";
import counterModel from "./counter";

const _debug = debug("models", "tourType");

//데이터 모델링
const tourTypeSchema = mongoose.Schema({
	name: { type: String, unique: true },
	code: { type: Number, unique: true }
});

class TourTypeClass {
	static async findByName(name) {
		return this.findOne({ name });
	}

	static async findByCode(code) {
		return this.findOne({ code });
	}

	static async deleteByName(name) {
		return this.deleteOne({ name: name });
	}
}

tourTypeSchema.pre("save", async function (next) {
	const doc = this;

	const counter = await counterModel.increament("tourType");

	doc.code = counter.seq;
	next();
});

tourTypeSchema.loadClass(TourTypeClass);
const tourTypeModel = mongoose.model("TourType", tourTypeSchema);

export default tourTypeModel;
