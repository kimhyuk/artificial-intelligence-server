import mkdirp from "mkdirp-promise";
import path from "path";
import shortid from "shortid";
import fs from "fs";

const defaultPath = path.join(path.dirname(require.main.filename), "..");

const uploadPath = "/uploads";

const subStringInTriplicate = function (str) {
	if (typeof str !== "string") {
		console.error("not String");
		return [];
	}

	const length = str.length / 3;
	let arr = [];

	for (let i = 0; i < length; i++) {
		arr.push(str.substr(i * length, length));
	}

	return arr;
};

export default async ({ basePath, createReadStream, filename }) => {
	let tmpPath = uploadPath.slice(0);

	if (typeof filename !== "string") {
		filename = shortid.generate();
	}

	if (typeof basePath !== "string") {
		basePath = defaultPath;
	}
	const str = String(Date.now()).substr(4);
	const pathArray = subStringInTriplicate(str);
	let filePath = basePath;

	for (let i = 0; i < pathArray.length; i++) {
		tmpPath = path.join(tmpPath, pathArray[i]);
	}

	filePath = path.join(basePath, tmpPath);

	await mkdirp(filePath);

	console.log(filePath);
	filePath = path.join(filePath, filename);

	return new Promise((resolve, reject) =>
		createReadStream()
			.pipe(fs.createWriteStream(filePath))
			.on("finish", () => resolve({ filename, path: tmpPath }))
			.on("error", reject)
	);
};
