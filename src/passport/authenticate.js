import passport from "passport";
import debug from "debug";

const _debug = debug("passport:auth");

export default (req, res, next) => {
	passport.authenticate("jwt", { session: false }, (err, user, info) => {
		if (user) {
			req.user = user;
		}
		_debug(req.header);
		next();
	})(req, res, next);
};
