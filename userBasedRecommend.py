from math import sqrt
import csv
import pymongo
import pprint
import sys
from bson import ObjectId

client = pymongo.MongoClient('mongodb://root:example@localhost:27017/admin')
db = client.test

userModel = db.users
vote_critics = {}

for doc in userModel.find():
    vote_critics[doc["_id"]] = {}
    for vote in doc["votes"]:
        vote_critics[doc["_id"]].update(
            {vote["touristDestination"]: vote["score"]})


def sim_distance(prefs, person1, person2):
    si = {}
    for item in prefs[person1]:
        if item in prefs[person2]:
            si[item] = 1
    if len(si) == 0:
        return 0

    sum_of_squares = sum([pow(prefs[person1][item]-prefs[person2][item], 2)
                          for item in prefs[person1] if item in prefs[person2]])

    return 1/(1+sum_of_squares)


def sim_pearson(prefs, p1, p2):
    si = {}
    for item in prefs[p1]:
        if item in prefs[p2]:
            si[item] = 1

    n = len(si)
    if n == 0:
        return 0

    sum1 = sum([prefs[p1][it] for it in si])
    sum2 = sum([prefs[p2][it] for it in si])

    sum1Sq = sum([pow(prefs[p1][it], 2) for it in si])
    sum2Sq = sum([pow(prefs[p2][it], 2) for it in si])

    pSum = sum([prefs[p1][it]*prefs[p2][it] for it in si])

    num = pSum-(sum1*sum2/n)
    den = sqrt((sum1Sq-pow(sum1, 2)/n)*(sum2Sq-pow(sum2, 2)/n))
    if den == 0:
        return 0

    r = num/den

    return r


# 다른 사람과의 순위의 가중평균값을 이용해서 특정 사람에게 추천
def getRecommendations(prefs, person, similarity=sim_pearson):
    totals = {}
    simSums = {}
    for other in prefs:
        # 나와 나를 비교하지 말것
        if other == person:
            continue
        sim = similarity(prefs, person, other)
        # 0 이하 점수는 무시함
        if sim <= 0:
            continue
        for item in prefs[other]:
            # 내가 보지 못한 관광지
            if item not in prefs[person] or prefs[person][item] == 0:
                # 유사도 * 점수
                totals.setdefault(item, 0)
                totals[item] += prefs[other][item]*sim
                simSums.setdefault(item, 0)
                simSums[item] += sim

    # 정규화된 목록 생성
    rakings = [(total/simSums[item], item) for item, total in totals.items()]
    rakings.sort()
    rakings.reverse()
    return rakings


prediction_votes = []
for re in getRecommendations(vote_critics, ObjectId(sys.argv[1])):
    prediction_votes.append(
        {"_id": ObjectId(), "touristDestination": re[1], "score": re[0]})

userModel.update({"_id": ObjectId(sys.argv[1])}, {
                 "$set": {"predictionVotes": prediction_votes}}, multi=True)

# print(userModel.find_one({"_id": ObjectId(sys.argv[1])}))
# f = open('result.csv', 'r')
# rdr = csv.reader(f)

# arrays = []
# for names in rdr:
#     arrays.append(names[1:])

# music_critics = {}
# names = [arrays[0][1:]]
# for key,arr in enumerate(arrays[1:]):
#     music_critics[arr[0]] = {}
#     for key2,val in enumerate(arr[1:]):
#         music_critics[arr[0]].update({names[0][key2]:int(val)})


# f.close()

# print(sim_pearson(music_critics,'20122941_김혁','20123011 송승환'))
